<?php


/**
 * Implementation of hook_drush_command().
 */
function drush_env_drush_command() {
  $items['en-env'] = array(
    'description' => 'Enable settings for a specific environment.',
    'arguments' => array(
      'environment-name' => 'A predefine environment name created in your drushrc.php file.',
    ),
    'examples' => array(
      'drush en-env development' => 'Enabled the development environment.',
    ),
  );

  return $items;
}



function drush_drush_env_en_env($environment) {
  if ($environment == NULL) {
    //error
  }
  $enviroments_options = drush_get_option('environments');
  if (isset($enviroments_options[$environment])) {
    foreach ($enviroments_options[$environment] as $settings => $options) {
      $function = "_drush_env_{$settings}_management";
      if (function_exists($function)) {
        $function($options);
      }
    }
  }
}

function _drush_env_modules_management($modules = array()) {
  // Lets build a list of modules to enable and a separate list of modules to disable.
  $enable = $disable = array();
  foreach ($modules as $module_name => $status) {
    if ($status == 1) {
      $enable[] = $module_name;
    }
    else {
      $disable[] = $module_name;
    }
  }
  if (!empty($enable)) {
    drush_invoke('pm-enable', implode(',', $enable));
  }
  
  if (!empty($disable)) {
    drush_invoke('pm-disable', implode(',', $disable));
  }
}

function _drush_env_settings_management($setting = array()) {
  foreach ($setting as $variable => $value) {
    drush_invoke('variable-set', $variable, $value);
  }
}
