<?php

// The option key MUST be enviroments.
$options['environments'] = array(
  // #key = The enviroment name.
  'development' => array(
    // The list of modules to enabled (1) or disable (0).
    'modules' => array(
      'views_ui' => 1,
      'context_ui' => 1,
      'devel' => 1,
      'theme_developer' => 0,
    ),
    // The list of variable to configure 
    'settings' => array(
      'preprocess_css' => 0,
      'preprocess_js' => 0,
    ),
  ),
);