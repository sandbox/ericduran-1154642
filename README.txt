
Drush environments
----------
Drush environments is an extension to drush that allows your to have pre-define environments
that you can turn on with a simple command. 

Among drush environments's capabilities are:

- Enabling and Disabling modules.
- Changing Variable Settings.


Usage
-----
The `drush en-env` command can be executed from a path within a Drupal.

    drush en-env [environment-name]

### Examples
    
    drush en-env development
    drush en-env development --y // Doesn't prompt

The `drush environments` file format
-----------------------
Each environments is an option define in your drushrc.php file. For an example of what the 'drush enviroments' 
module expects please see the enviroments.examples.drushrc.php file.

See http://drupalcode.org/project/drush.git/blob/refs/heads/master:/examples/example.drushrc.php for
more info on drushrc.php files.

